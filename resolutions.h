// Electron 
Double_t I_beam      = 1900.0;  // A (Torus current?) !

//// Resolutions
Double_t dP_dc       = 3.40;    // MeV
Double_t dTheta_dc   = 2.50;    // Deg.
Double_t dPhi_dc     = 4.00;    // Deg.

// IC 
//// Resolutions
Double_t dE_ic       = 1.33;    // %
Double_t dX_ic       = 1.20;    // cm

// RTPC 
//// Resolutions
Double_t dP_rtpc     = 0.10;    // %/100
Double_t dTheta_rtpc = 4.00;    // Deg.
Double_t dPhi_rtpc   = 4.00;    // Deg.

// Other
Double_t dVz_e       = 0.30;    // cm         !?
Double_t dVz_he4     = 0.46;    // cm !!


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<Double_t> getResErr_DC( TLorentzVector P_e ){
  Double_t p     = P_e.P();
  Double_t theta = P_e.Theta() * TMath::RadToDeg();
  Double_t beta  = P_e.Beta();
  
  const double sigma_p     = dP_dc     * p * pow(theta/35,0.7) * sqrt(pow(0.0033*p,2)+pow(0.0018/beta,2)) * 3375/I_beam;
  const double sigma_theta = dTheta_dc * sqrt(pow(0.55,2)+pow(1.39/p/beta,2)) / 1000; // mrad
  const double sigma_phi   = dPhi_dc   * sqrt(pow(3.73,2)+pow(3.14/p/beta,2)) / 1000; // mrad
  
  //cout<<endl;
  //cout << sigma_p<< endl;
  //cout << sigma_theta*TMath::RadToDeg() << endl;
  //cout << sigma_phi*TMath::RadToDeg() << endl;
  //1.2*sqrt(0.003^2/6 + (0.13*90)^2)
  //std::vector<Double_t> sigmas = {sigma_p, sigma_theta, sigma_phi};
  std::vector<Double_t> sigmas = {sigma_p, sigma_theta, sigma_phi};
  return sigmas;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<Double_t> getResErr_IC( TLorentzVector P_phot, Double_t vz = 0 ){
  Double_t E     = P_phot.E();///1000;
  Double_t theta = P_phot.Theta();
  
  const double sigma_E     = dE_ic * E * sqrt(pow(0.024,2)+pow(0.033/sqrt(E),2)+pow(0.019/E,2));
  const double sigma_theta = dX_ic * sqrt(pow(0.003/sqrt(E),2)+pow(0.013*theta,2));
  const double sigma_phi   = dX_ic * 0.003/sqrt(E);

//cout<<endl;
//cout << sigma_E<< endl;
//cout << sigma_theta*TMath::RadToDeg() << endl;
//cout << sigma_phi*TMath::RadToDeg() << endl;
  //1.2*sqrt(0.003^2/6 + (0.13*90)^2)
  std::vector<Double_t> sigmas = {sigma_E, sigma_theta, sigma_phi};
  
  return sigmas;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<Double_t> getResErr_RTPC( TLorentzVector P_he4 ){
  Double_t p     = P_he4.P();
  Double_t theta = P_he4.Theta() * TMath::RadToDeg();
  Double_t beta  = P_he4.Beta();
  
  const double sigma_p     = dP_rtpc     * p;
  const double sigma_theta = dTheta_rtpc * TMath::DegToRad() ;
  const double sigma_phi   = dPhi_rtpc   * TMath::DegToRad() ;

  std::vector<Double_t> sigmas = {sigma_p, sigma_theta, sigma_phi};
  return sigmas;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<Double_t> getResErr_EC( TLorentzVector P_phot, Double_t vz = 0 ){
  Double_t E     = P_phot.E();///1000;
  
  const double sigma_E     = 0.116 * sqrt(E);
  const double sigma_theta = 0.004;
  const double sigma_phi   = 0.004;
    
  std::vector<Double_t> sigmas = {sigma_E, sigma_theta, sigma_phi};
  
  return sigmas;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

