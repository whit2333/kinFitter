
## To compile

```
mkdir build
cd build
cmake ../. -DCMAKE_INSTALL_PREFIX=$HOME # or wherever
```

## Using in ROOT

Add the following to your `.rootlogon.C` to automatically load library
```
gSystem->Load("libKinFitter2"); // $HOME/lib needs to be in LD_LIBRARY_PATH
gInterpreter->AddIncludePath("$HOME/include");
```

## Testing 

`tests/generate_test_data.cxx`: 

This creates test dataset using `TGenPhaseSpace`. TDataFrame is used.


```
cd tests
root -x generate_test_data.cxx++
```

